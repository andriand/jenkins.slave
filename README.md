

get jenkins dockerized master and run it

    $ docker pull jenkins
    $ docker run -d --name jenkins.master -p 8888:8080 -p 50000:50000 -v /Users/jenkins_dock:/var/jenkins_home jenkins

login jenkins master 
    
    $ open http://$(docker-machine ip $DOCKER_MACHINE_NAME):8888

on Jenkins master:
 - create admin user
 - install swarm slave plugin
 - define necessary git credentials
 - create pipeline job that takes Jenkinsfile from repository

export SWARM_LOGIN_USERNAME and SWARM_LOGIN_PASSWORD

run dockerized jenkins swarm slave and register it on running jenkins.master instance

    $ docker run -d --name jenkins.slave -v /var/run/docker.sock:/var/run/docker.sock --env SWARM_CMD_MASTER_URL=http://$(docker inspect --format='{{.NetworkSettings.IPAddress}}' jenkins.master):8080  SWARM_CMD_USERNAME="${SWARM_CMD_USERNAME}" SWARM_CMD_PASSWORD="${SWARM_CMD_PASSWORD}" jenkins.slave


alternative way to run 

    $ docker compose up &

